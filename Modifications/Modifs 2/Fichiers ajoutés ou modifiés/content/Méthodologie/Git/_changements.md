---
title: "Enregistrer des changements"
weight: 4
date: 2022-01-11T14:19:27Z
draft: false
---

### 
La notion d'enregistrement est plus complexe et flexible dans Git qu'avec un système de fichiers traditionnel. Pour bien le comprendre, il faut savor que Git maintient 3 états internes:  
* le répertoire de travail;
* l'index;
* l'historique des commits.

Le répertoire de travail est un espace synchronisé avec le système de fichier de votre poste de travail. Chaque modification (ajout, suppression, modification d'un fichier) se répercute dans le répertoire de travail.  

L'index correspond à l'ensemble des fichiers sélectionnés pour faire partie du prochain commit. On ajoute des fichiers à l'index avec la commande *add*. 

La commande git commit capture un instantané des changements indexés. Tous les commits sont gardés dans un historique. Git garde un pointeur sur la référence d'un commit courant, appelé HEAD. En temps normal, HEAD est positionné sur le dernier commit effectué.  

**États des fichiers dans Git**  
![git etats 1](http://ml.cm9.ca/cours3d1/images/etats1.png)  

La commande *status* affiche l'état de Git:  
![git status 1](http://ml.cm9.ca/cours3d1/images/gitStatus1.png)  
Le statut indique que le répertoire de travail ne contient aucun changment, l'index est vide et la branche est à jour avec la branche équivalente sur origine.  

La commande *log* affiche l'historique des commmits exécutés:  
![git log 1](http://ml.cm9.ca/cours3d1/images/gitLog1.png)  

La commande affiche les commits en ordre chronologique inverse. On voit que la branche *main* du dépôt local est synchronisée avec celle du dépôt *origine*.  

Si on ajoute le fichier *Deuxieme.txt* au dossier et que l'on modifie le fichier *Premier.txt*, status affiche le résultat suivant:  
![git status 2](http://ml.cm9.ca/cours3d1/images/gitStatus2.png)  
Git a détecté que le fichier *Premier.txt* a été modifié. La couleur rouge indique que le fichier n'a pas été indexé. *Deuxieme.txt* a le statut *untracked* qui indique que le fichier ne fait pas encore partie du répertoire de travail de Git.  

### La séquence de travail habituelle: Édition, Add, Commit
La commande *add* ajoute les fichiers spécifiés à l'index:  
![git status 3](http://ml.cm9.ca/cours3d1/images/gitStatus3.png)  
Le statut indique qu'un nouveau fichier est ajouté à l'index. Si on ajoute à l'index le fichier modifié, on obtient le résultat suivant:  
![git status 4](http://ml.cm9.ca/cours3d1/images/gitStatus4.png)  
**Note**: si de nouvelles modifications sont faites sur le fichier, il faut refaire un autre *add* pour que le commit suivant tiennent compte des nouvelles modifications.  

Un commit enregistre toutes les modifications indexées dans l'historique des commits:  
![git status 5](http://ml.cm9.ca/cours3d1/images/gitStatus5.png)  
Le satut nous indique que le répertoire de travail ne contient pas de changements et l'index est vide. On voit aussi que que notre branche locale est 1 commit "en avance" sur la branche *origin/main*. Le dépôt distant n'est pas à jour par rapport à notre dépôt local. Un *push* de la branche *main* synchronisera *origin* avec notre dépôt local:
![git status 6](http://ml.cm9.ca/cours3d1/images/gitStatus6.png)  

**Retirer des fichiers de l'index**  
La commande *reset* retire un ou des fichiers de l'index, sans modifier le répertoire de travail. Ce qui signifie que les modifications apportées à ces fichiers ne sont pas perdues, mais que ces fichiers ne feront pas partie du prochain commit.  

```posh
>git reset doc1.txt 
```

**Annuler les changements d’un fichier dans le répertoire de travail**  
La commande *restore* réinitialise un ou des fichiers du répertoire de travail à leur état avant l'édition. Ce qui signifie que les modifications apportées à ces fichiers seront perdues.  

```posh
>git restore doc1.txt
```

**Modifier le dernier commit**  
On peut modifier le dernier commit pour y ajouter de nouvelles modifications ou simplement changer le message de commit.  Dans l'exemple suivant, seulement le message de commit est amendé.  

```posh
>git commit --amend -m "Nouveau message de commit"
```

Dans le prochain exemple, une nouvelle modification est apportée au fichier *doc1.txt* et le message de commit est modifié.  

```posh
>echo Édition du fichier doc1.txt...
>git add doc1.txt
>git commit --amend -m "Nouveau message de commit"
```

**Inspecter l’historique des commits**  
*Git log --oneline* affiche l'historique des commits à raison d'une ligne par commit.  
![gitLogOneline](http://ml.cm9.ca/cours3d1/images/gitLogOneline.png)  

**Ramener un ancien commit dans le répertoire de travail**  
Par défaut, le répertoire de travail contient l'état du dernier commit, en plus des modifications que vous avez apportées aux fichiers. Un pointeur, HEAD, est conservé sur le dernier commit de la branche sur laquelle vous travaillez. Il est possible de ramener l'état d'un ancien commit dans le répertoire de travail par la commande *checkout*. Dans ce cas, le pointeur HEAD ne pointe plus sur votre branche mais sur un commit particulier de l'historique. On dit que que git est alors dans un état "HEAD détaché".  

```posh
>git checkout e0eeac0
```
Nous pouvons mainenant inspecter les changements faits lors de ce commit. Pour ramener le pointeur HEAD sur la branche de travail, on utilise la commande *checkout* avec le nom de la branche:    
```posh
>git checkout main
```
**Sauvegarder le répertoire de travail**  
Puisqu'un *checkout* ramène un commit dans le répertoire de travail, le répertoire de travail doit être exempt de modifications. On peut sauvegarder l'état du répertoire de travail temporairement de façon à pouvoir effectuer un checkout.  
La commande *stash* sauvegarde temporairement le répertoire de travail.  
La commande *stash pop* restaure les modifications sauvegardée dans le stash.  
Voici la séquence habituelle de commandes:   

```posh
>git stash
>git checkout <numéro de commit> 
>git checkout main  
>git stash pop
```
